if [ $# -ne 2 ]
  then echo invalid [arg1:robot_name:abc.robot] [arg2:result_path:../result/]
  exit 1
fi

robot_name=$1
result_path=$2

robot_log=$result_path$robot_name-log.html
robot_report=$result_path$robot_name-report.html
robot_output=$result_path$robot_name-output.xml

echo "log    : "_$robot_logfile
echo "report : "_$robot_report
echo "output : "_$robot_output
robot --log $robot_log --output $robot_output --report $robot_report $robot_name
