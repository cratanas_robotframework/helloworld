*** Settings ***
Documentation    Search Google WebSite by HelloWorld Word
...              for expect Display Page Contains HelloWorld
Library          Selenium2Library
Resource         ../resource/res_common.robot
Resource         ../resource/res_helloworld.robot


*** Test Case ***
Search HelloWorld
    [Documentation]             Search Google WebSite by HelloWorld Word
    [Tags]                      TC_helloworld_001
    Open Goolge WebSite
    Input Search Word
    Wait Page Contain HelloWorld
    [Teardown]                  Close Browser
    