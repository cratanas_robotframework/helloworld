*** Settings ***
Library     Selenium2Library     

*** Variable ***
${Search_Word}      HelloWorld 
${Expect_Result}    HelloWorld

*** Keywords ***
Open Goolge WebSite
    Open Browser    ${Url_google}   ${Browser_Default}

Input Search Word
    Input Text      name=q          ${Search_Word}
    Press Key       name=q	        \\13

Wait Page Contain HelloWorld
    Wait Until Page Contains    ${Expect_Result}

